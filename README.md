# AWS Lambda Layers

## How to create a layer

Launch an Docker container like this:
```shell script
docker run --rm -it -v "$PWD:/code" lambci/lambda:build-python3.8 sh
```

Generateyour virtual environment
```shell script
cd /code
virtualenv env
source env/bin/activate
``` 

Install your library
```shell script
pip install <library>
```

zip folders in pip install pillow
```shell script
cp -r env/lib/python3.8/site-packages python
rm -rf python/_distutils_hack
rm -rf python/distutils-precedence.pth
rm -rf python/easy_install.py
rm -rf python/pip
rm -rf python/pip-20.2.4.dist-info
rm -rf python/pip-20.2.4.virtualenv
rm -rf python/pkg_resources
rm -rf python/__pycache__
rm -rf python/setuptools
rm -rf python/setuptools-50.3.2.dist-info
rm -rf python/setuptools-50.3.2.virtualenv
rm -rf python/_virtualenv.pth
rm -rf python/_virtualenv.py
rm -rf python/wheel
rm -rf python/wheel-0.35.1.dist-info
rm -rf python/wheel-0.35.1.virtualenv
zip -r /layers/python-3.8-boto3.zip python
```
