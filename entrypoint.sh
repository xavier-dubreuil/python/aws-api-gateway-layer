#!/bin/bash

VERSION=$(python --version | cut -d' ' -f2 | cut -d'.' -f1,2)
PACKAGE=$1

mkdir -p /usr/src/layers /usr/src/env

cd /usr/src/env

virtualenv env
source env/bin/activate

pip install "${PACKAGE}"

cp -r env/lib/python3.8/site-packages python

rm -rf python/_distutils_hack
rm -rf python/distutils-precedence.pth
rm -rf python/easy_install.py
rm -rf python/pip
rm -rf python/pip-*.dist-info
rm -rf python/pip-*.virtualenv
rm -rf python/pkg_resources
rm -rf python/__pycache__
rm -rf python/setuptools
rm -rf python/setuptools-*.dist-info
rm -rf python/setuptools-*.virtualenv
rm -rf python/_virtualenv.pth
rm -rf python/_virtualenv.py
rm -rf python/wheel
rm -rf python/wheel-*.dist-info
rm -rf python/wheel-*.virtualenv

zip -r "/usr/src/layers/python-${VERSION}-${PACKAGE}.zip" python
