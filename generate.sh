#!/bin/bash

VERSION=$1
PACKAGE=$2

docker run --rm \
   -v "$PWD/layers:/usr/src/layers" \
   -v "$PWD/entrypoint.sh:/usr/local/bin/entrypoint" \
   --entrypoint "/usr/local/bin/entrypoint" \
   "lambci/lambda:build-python${VERSION}" "${PACKAGE}"
